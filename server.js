// load the things we need
var express = require('express');
var app = express();

// set the view engine to ejs
app.set('view engine', 'ejs');

// using app.use to serve up static CSS files in public/assets/ folder when /public link is called in ejs files
// app.use("/route", express.static("foldername"));
app.use(express.static('public/assets'));

// use res.render to load up an ejs view file

// index page 
app.get('/', function(req, res) {
    res.render('pages/index');
});
// creacompteuser page
app.get('/creacompteuser', function (req,res) {
    res.render('pages/creacompteuser');
});
//page défi
app.get('/defi', function (req, res) {
	res.render('pages/defi');
});

//page défi
app.get('/defiliste', function (req, res) {
	res.render('pages/defiliste');
});

//page admin
app.get('/admin', function (req, res) {
	res.render('pages/admin');
});


// ajoutdefiuser page 
app.get('/ajoutdefiuser', function(req, res) {
    res.render('pages/ajoutdefiuser');
});

app.listen(8080);
console.log('8080 est le port magique');

